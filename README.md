# weatherr
   
## Build Steps
   
Run `cargo build --release` for building the project, or `sudo cargo install --path . --root /usr/` to install it system-wide.
   
## Usage

`weather <Location> <format>`
