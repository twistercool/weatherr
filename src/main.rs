use std::env;
use std::fs::File;
use std::path::PathBuf;
use std::time::{SystemTime, Duration};
use std::io::{BufReader, BufRead, Write};
use std::error::Error;
use std::process;

use reqwest;

fn main() {
    let argv: Vec<String> = env::args().collect();

    if argv.len() != 3 {
        eprintln!("Usage: {} <Location> <format>", argv[0]);
        process::exit(1);
    }

    let url = format!("https://wttr.in/{}?format='{}'", &argv[1], &argv[2]);

    let _cached_value = match get_cache(&url) {
        Ok(str) => {
            println!("{}", &str[1..str.len()-1]);
            process::exit(0)
        },
        Err(e) => e,
    };

    let response = match reqwest::blocking::get(&url) {
        Ok(resp) => {
            resp.text().expect("Couldn't create string")
        }
        Err(err) => {
            eprintln!("Error: {}", err);
            //put in cache that it's nothing
            match put_in_cache(&url, "") {
                Ok(_) => process::exit(0),
                Err(e) => {
                    eprintln!("Error putting in cache: '{e}'");
                    process::exit(1);
                }
            }
        }
    };

    //Don't show the quotes around
    println!("{}", &response[1..response.len()-1]);

    if let Err(e) = put_in_cache(&url, &response) {
        eprintln!("Error putting in cache: '{e}'");
        process::exit(1);
    }
}



#[derive(Debug)]
enum WeatherError {
    Error(&'static str)
}

impl Error for WeatherError {}

impl std::fmt::Display for WeatherError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            WeatherError::Error(s) => {
                write!(f, "Error: '{s}'")
            }
        }
    }
}

fn get_cache(url: &str) -> Result<String, Box<dyn Error>> {
    let mut path = PathBuf::from(std::env::var("HOME").unwrap());
    path.push(".cache");
    path.push("weatherr");

    let metadata = std::fs::metadata(path.as_path())?;
    let modified_time = metadata.modified()?;
    let elapsed_time = SystemTime::now().duration_since(modified_time)?;
    if elapsed_time <= Duration::from_secs(3600) {
        let file = File::open(path)?;
        let mut reader = BufReader::new(file);
        let mut prev_url = String::new();
        let mut returned = String::new();
        let _len = reader.read_line(&mut prev_url)?;
        if &prev_url[0..prev_url.len()-1] != url {
            return Err(Box::new(WeatherError::Error("Cached url doesn't match the previous one")));
        }
        for line in reader.lines() {
            let line = match line {
                Ok(str) => str,
                Err(_) => std::process::exit(1)
            };
            returned.push_str(&line);
        }
        return Ok(returned);
    } else {
        Err(Box::new(WeatherError::Error("Cache is old")))
    }
}

fn put_in_cache(url: &str, body: &str) -> Result<(), Box<dyn Error>> {
    let mut path = PathBuf::from(std::env::var_os("HOME").unwrap());
    path.push(".cache");

    std::fs::create_dir_all(&path)?;
    path.push("weatherr");

    let mut file = File::create(path)?;
    file.write_all(url.as_bytes())?;
    file.write_all(b"\n")?;
    file.write_all(body.as_bytes())?;

    Ok(())
}
